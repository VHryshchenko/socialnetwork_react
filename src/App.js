// import logo from './logo.svg';
import './App.css';
import {
    BrowserRouter,
    Routes,
    Route
} from "react-router-dom";
import Header from "./components/header/header";
import Footer from "./components/footer/footer";
// import Navbar from "./components/body/navbar/navbar";
// import Profile from "./components/body/content-part/Profile/profile";
import Body from "./components/body/body";
import Navbar from "./components/body/navbar/navbar";
import Content from "./components/body/content-part/content_part";
import React from "react";
import Profile from "./components/body/content-part/Profile/profile";
import Messages from "./components/body/content-part/Messages/messages";
import Music from "./components/body/content-part/Music/music"
import Settings from "./components/body/content-part/Settings/settings";
import News from "./components/body/content-part/News/news";
import Picture from "./components/body/content-part/Profile/picture/picture";

const App = (props) => {
  return (
<BrowserRouter>

    <div className='app-wrapper'>
        <div className='left-part'>

        </div>
             <div className='central-part'>

                     <Header />
                <div className='body'>

                    <Navbar />
                 {/*<Content />*/}

                    <Routes>

                    <Route  element={<Profile />} path="/profile" />
                    <Route  element={<Messages />} path="/messages"/>
                        <Route  element={<Music />} path="/messages/:id"/>
                        {/*<Route  element={<Picture />} path="/picture"/>*/}
                        {/*<Route  element={<News />} path="/messages/vasiliy"/>*/}
                        {/*<Route  element={<News />} path="/messages/andrew"/>*/}
                        {/*<Route  element={<News />} path="/messages/alex"/>*/}
                    <Route  element={<News />} path="/news"/>
                    <Route  element={<Music />} path="/music"/>
                    <Route  element={<Settings />} path="/setting"/>


                    </Routes>
                </div>

                     <Footer  />

              </div>
        <div className='right-part'>

        </div>
    </div>
</BrowserRouter>
  );
}





export default App;


