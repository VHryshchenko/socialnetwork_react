import React from 'react';
import './navbar.css'
import {NavLink} from "react-router-dom";

const Navbar = () => {
    return (
        <div className='navbar'  >
            <div> <a href="/profile"> Profile </a> </div>
            <div> <NavLink to="/messages"> Messages </NavLink></div>
            <div> <a href="/news"> News </a></div>
            <div> <a href="/music"> Music </a></div>
            <div> <a href="/settings"> Settings </a> </div>

        </div>
    );
}

export default Navbar;

