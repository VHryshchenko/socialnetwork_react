import React from "react";
import Navbar from "./navbar/navbar";
import Content from "./content-part/content_part";
import  "./body.css";


const Body = () => {
    return(
        <div className='body'>
            <Navbar />
            <Content />

        </div>
    )
}
export default Body;
