import React from "react";
import './profile.css';
import Picture from "./picture/picture";
import Posts from "./posts/posts";
import Personal_info from "./personal_info/personal_info";


const  Profile = () => {
    return (
    <div className='profile'>
        <Picture />
        <Personal_info />
        <Posts />
    </div>)
}

export default Profile;
