import React from "react";
import './messages.css';
import {NavLink} from "react-router-dom";
import {useParams} from "react-router-dom";


const DialogItem = (props) => {
    let path = "/messages/" +props.id;
    return(
        <div> <NavLink to = {path}> {props.name} </NavLink></div>
    )
}

const MessagesComponent = (props) => {
    const { id } = useParams();
    console.log("props.location", id)
    return(
      <div> {props.message} </div>
    )
}

const TextMessage = (props) => {
return(
   <div> {props.message} </div>
)
}

const  Messages = () => {
  // DataLayer
    // this is author array
    let DialogData = [
        {id: 'ivan', name: "Ivan "},
        {id: 'vasiliy', name: "Vasiliy" },
        {id: 'andrew', name: "Andrew "},
        {id: 'alex', name: "Alex "},
    ]
    // this is messages array
    let MessageData = [
        {id: 0, message: "hey you!" },
        {id: 1, message: "How are you?!" },
        {id: 2, message: "London is a Capital of GB! "},
        {id: 3, message: "NY is a biggest city over the wold! "},
    ]

    // let Dialitems = DialogData.map( (id) => <DialogItem name={DialogData.name} id={DialogData.id} /> )
    let Friends = DialogData.map( (element) => {
    console.log(element)
        return <DialogItem key={element.id} name={element.name} id={element.id}/>
    })

    let Phrase = MessageData.map( (el) => {
        console.log(el)
        return <MessagesComponent key={el.id} message={el.message}/>
    })

    return (
        <div className='messageblock'>
                <div className='author'>
                    {Friends}
                </div>

                <div className='messages'>
                   {/*<TextMessage message={MessageData[0].message} />*/}
                   {/*<TextMessage message={MessageData[1].message} />*/}
                   {/*<TextMessage message={MessageData[2].message} />*/}
                   {/*<TextMessage message={MessageData[3].message} />*/}
                    {Phrase}
                </div>
        </div>
    )
}

export default Messages;
